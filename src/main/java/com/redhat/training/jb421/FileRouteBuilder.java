package com.redhat.training.jb421;

import org.apache.camel.builder.RouteBuilder;

public class FileRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		from("file://origin")
		.to("file:etc/file/out?fileName=output.xml&fileExist=Append");
		
	}

}
